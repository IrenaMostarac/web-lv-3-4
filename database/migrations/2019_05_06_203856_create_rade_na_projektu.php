<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRadeNaProjektu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rade_na_projektu', function (Blueprint $table) {
            $table->text('id_radnika')->references('id')->on('users');
            $table->text('id_projekta')->references('id')->on('projects');        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rade_na_projektu');
    }
}
