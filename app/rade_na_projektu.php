<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rade_na_projektu extends Model
{
    protected $fillable = ['id_radnika','id_projekta'];
	public $timestamps = false;
}
