<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class projects extends Controller
{
    protected function redirectTo()
    {
        return '/home';
    }
    public function InsertProject(Request $request){	
        $NewProject = new Project;
        $NewProject->naziv projekta=$request->naziv projekta;
        $NewProject->opis projekta=$request->input('opis projekta');
        $NewProject->cijena projekta=$request->input('cijena projekta');
        $NewProject->obavljeni_poslovi=$request->input('obavljeni_poslovi');
        $NewProject->datum_početka=$request->input('datum_početka');
        $NewProject->datum_završetka=$request->input('datum_završetka');
        $NewProject->id_voditelja=Auth::id();
        $NewProject->save();
        return view('welcome');	
    }
    public function edit(Request $changerequest){
        $authID= Auth::id();
        $selectedProject = new Project;
        $selectedProject= \App\Project::find( $changerequest->id);
        $selectedProject->naziv projekta=$changerequest->naziv projekta;
        $selectedProject->opis projekta=$changerequest->opis projekta;
        $selectedProject->cijena projekta=$changerequest->cijena projekta;
        $selectedProject->obavljeni_poslovi=$changerequest->obavljeni_poslovi;
        $selectedProject->datum_početka=$changerequest->datum_početka;
        $selectedProject->datum_završetka=$changerequest->datum_završetka;
        $selectedProject->id_voditelja=$changerequest->id_voditelja;
        $selectedProject->save();
  	
    if($authID==$changerequest->id_voditelja){
        $rade_na_projektu = \App\rade_na_projektu::create(['id_projekta' =>  $changerequest->id , 'id_radnika' => $changerequest->human]);
    }
  	return view('welcome');	
  }
}
protected $fillable = ['id','naziv projekta','opis projekta','cijena projekta','obavljeni_poslovi','datum_početka','datum_završetka','id_voditelja']