<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class projects extends Model
{
    protected $table = 'projects';
    protected $fillable = ['id','naziv projekta','opis projekta','cijena projekta','obavljeni_poslovi','datum_početka','datum_završetka','id_voditelja'];
    public $timestamps = false;
}
